﻿const hexToBinary = require('hex-to-binary');
const { GENESIS_DATA, MINE_RATE } = require("../config");
const cryptoHash = require("../Util/crypto-hash");

class Block {
  constructor({ timestamp, lasthash, data, hash, nonce, difficulty }) {
    this.timestamp = timestamp;
    this.lasthash = lasthash;
    this.hash = hash;
    this.data = data;
    this.nonce = nonce;
    this.difficulty = difficulty;
  }

  static genesis() {
    return new this(GENESIS_DATA);
  }

  static mineBlock({ lastBlock, data }) {
    //  create block beased on blovk input and represent last block for that block
    const lasthash = lastBlock.hash;
    let hash, timestamp;
    let { difficulty } = lastBlock;
    let nonce = 0;
 
    do {
      nonce++;
      timestamp = Date.now();
      difficulty = Block.adjustDifficulty({originalBlock: lastBlock,timestamp})
      hash = cryptoHash(timestamp, lasthash, data, nonce, difficulty);
    } while (hexToBinary(hash).substring(0, difficulty) !== "0".repeat(difficulty));

    return new this({
      timestamp,
      lasthash,
      difficulty,
      nonce,
      data,
      hash,
    });
  }
  static adjustDifficulty({ originalBlock, timestamp }) {
    const { difficulty } = originalBlock;

    const difference = timestamp - originalBlock.timestamp;

    if(difficulty < 1) return 1 

    if (difference > MINE_RATE) {
      return difficulty - 1;
    }
    return difficulty + 1;
  }
}

module.exports = Block;
