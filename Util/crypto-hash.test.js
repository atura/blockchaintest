﻿const cryptoHash = require('./crypto-hash')

describe('cryptoHash()', () => { 
    it('generate a SHA-256 hased output',()=>{
        expect(cryptoHash('amini')).toEqual('5575830157184c0d6924d101b675e60822541177959b31ccfea2cada03ed2064')})

    it('produce the same hash with the same input argument in any order',()=>{
        expect(cryptoHash('one','two','three')).toEqual(cryptoHash('three','one','two'))
    })
 })
