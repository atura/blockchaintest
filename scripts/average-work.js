﻿const Blockchain = require('../blockchain/index')

const blockchain = new Blockchain()

blockchain.addBlock({data:'initial'})

console.log('first block', blockchain.chain[blockchain.chain.length-1])

let prevTimestamp, nextTimestamp, timeDiff, average

const times = [];


for (let i=0 ; i<10000;i++){
  prevTimestamp = blockchain.chain[blockchain.chain.length-1].timestamp
  
  blockchain.addBlock({data:`block${i}`})
  nextBLock = blockchain.chain[blockchain.chain.length-1]

  nextTimestamp = nextBLock.timestamp
  timeDiff = nextTimestamp - prevTimestamp 
  times.push(timeDiff)

  average = times.reduce((total,num)=> (total+num))/times.length

  console.log(`Time to mine block : ${timeDiff}ms. difficulty: ${nextBLock.difficulty}. average time:${average}ms`)
}